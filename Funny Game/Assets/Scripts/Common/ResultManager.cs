﻿using Common.EventSystem;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Common
{
    public class ResultManager : EventListernerMonoBehaviour
    {
        [Header("Score options")]
        [SerializeField]
        private int startScore = 10, currentScore;

        [Header("UI Elements")]
        [SerializeField]
        private TextMeshProUGUI scoreTextBox, timeTextBox, loseWinText;
        [SerializeField]
        private GameObject loseWinPanel;
        [SerializeField]
        private TextMeshProUGUI reloadOrNextLevelTextBox;

        [Header("Time options")]
        [SerializeField]
        private int time = 30, currentTime = 0;

        [SerializeField]
        private CliclkDestroy cliclkDestroy;

        private int goodClicks = 0;

        private bool levelCanceled = false;

        void Start()
        {
            currentScore = startScore;
            scoreTextBox?.SetText(startScore.ToString());

            currentTime = time;
            timeTextBox?.SetText(currentTime.ToString());
            StartCoroutine("Timer");

            loseWinPanel?.SetActive(false);

            Time.timeScale = 1;

            cliclkDestroy = FindObjectOfType<CliclkDestroy>();
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.GameEventType == GameEvents.AddScore)
            {
                if (eventType.EventName == "MainScore")
                {
                    currentScore += (int)eventType.Arg;

                    scoreTextBox?.SetText(currentScore.ToString());

                    if (currentScore < 0)
                    {
                        CancelLevel();
                    }
                }

                if (eventType.EventName == "Time")
                {
                    currentTime += 3;

                    goodClicks = 0;

                    MMGameEvent.Trigger(GameEvents.AddScore, "Goodclicks", goodClicks);
                }
            }

            if (eventType.GameEventType == GameEvents.GoodClick)
            {
                goodClicks++;

                MMGameEvent.Trigger(GameEvents.AddScore, "Goodclicks", goodClicks);

                if (goodClicks == 5)
                {
                    MMGameEvent.Trigger(GameEvents.Bonus);
                }
            }

            if (eventType.GameEventType == GameEvents.LoseClick)
            {
                goodClicks = 0;
                MMGameEvent.Trigger(GameEvents.AddScore, "Goodclicks", goodClicks);
            }
        }

        private IEnumerator Timer()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);

                currentTime--;

                timeTextBox?.SetText(currentTime.ToString());

                if (currentTime == 0)
                {
                    CancelLevel();
                    yield break;
                }
            }
        }

        private void CancelLevel()
        {
            if (!levelCanceled)
            {
                if(cliclkDestroy != null)
                cliclkDestroy.enabled = false;
                
                levelCanceled = true;

                loseWinPanel?.SetActive(true);

                Time.timeScale = 0;

                if (currentScore > 0)
                {
                    loseWinText?.SetText("You WIN !!!");
                    reloadOrNextLevelTextBox?.SetText("Next Level");
                    MMGameEvent.Trigger(GameEvents.StateComplete);
                }
                else
                {
                    loseWinText?.SetText("You Lose (((");
                    reloadOrNextLevelTextBox?.SetText("Restart");
                    MMGameEvent.Trigger(GameEvents.LevelFailed);
                }
            }
        }
    }
}
