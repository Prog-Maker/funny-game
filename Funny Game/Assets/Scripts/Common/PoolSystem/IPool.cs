﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.PoolSystem
{
    public interface IPool
    {
        GameObject Take(Transform paren);

        void Return(GameObject gameObjectToReturn);
    }
}
