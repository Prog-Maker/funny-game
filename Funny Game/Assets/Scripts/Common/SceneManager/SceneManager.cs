﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using unitySM = UnityEngine.SceneManagement.SceneManager;

namespace Common
{
    public class SceneManager : EventListernerMonoBehaviour
    {
        public static SceneManager instance;

        [SerializeField]
        private AssetReference[] scenes;

        [SerializeField]
        private int currentSceneIndex = 0;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                if (instance != this)
                {
                    Destroy(gameObject);
                }
            }

            DontDestroyOnLoad(this);
        }

        public void ReloadScene()
        {
            Addressables.LoadSceneAsync(scenes[currentSceneIndex], LoadSceneMode.Single);
        }

        public void LoadScene(int index)
        {
            currentSceneIndex = index;
            Addressables.LoadSceneAsync(scenes[index], LoadSceneMode.Single);
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.GameEventType == GameEvents.StateComplete)
            {
                currentSceneIndex++;

                if (currentSceneIndex >= scenes.Length)
                {
                    currentSceneIndex = 0;
                }
            }

            if (eventType.GameEventType == GameEvents.LoadNextLevel)
            {
                ReloadScene();
            }

            if (eventType.GameEventType == GameEvents.EndGame)
            {
                if (unitySM.GetActiveScene().name != "Main")
                {
                    unitySM.LoadScene("Main");
                }
            }

            if (eventType.GameEventType == GameEvents.StartGame)
            {
                LoadScene(0);
            }
        }
    }
}
