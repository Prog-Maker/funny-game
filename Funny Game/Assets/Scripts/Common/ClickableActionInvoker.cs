﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Common
{
    public class ClickableActionInvoker : MonoBehaviour, IPointerClickHandler/*, IPointerDownHandler, IPointerUpHandler*/
    {
        [SerializeField]
        private UnityEvent actionToInvoke;

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log(name + " Game Object Clicked!");
            actionToInvoke?.Invoke();
        }

        //public void OnPointerDown(PointerEventData eventData)
        //{
        //    Debug.Log(name + " Game Object Down!!");
        //}

        //public void OnPointerUp(PointerEventData eventData)
        //{
        //    Debug.Log(name + " Game Object UP!!");
        //}
    }
}
