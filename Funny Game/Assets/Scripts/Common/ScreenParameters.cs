﻿using UnityEngine;

public class ScreenParameters : MonoBehaviour
{
    public static ScreenParameters Instance;
    public float MinX { get; private set; }
    public float MaxX { get; private set; }
    public float MinY { get; private set; }
    public float MaxY { get; private set; }

    private Camera _camera;

    private void Awake()
    {
        Instance = this;
        _camera = Camera.main;

        var leftBottom = _camera.ScreenToWorldPoint(Vector2.zero);
        var rightTop = _camera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));

        MinX = leftBottom.x;
        MinY = leftBottom.y;
        MaxX = rightTop.x;
        MaxY = rightTop.y;
    }
}
