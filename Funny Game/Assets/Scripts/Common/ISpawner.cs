﻿namespace Common
{
    public interface ISpawner
    {
        void Spawn();
    }
}
