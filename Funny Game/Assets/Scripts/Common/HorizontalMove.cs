﻿using UnityEngine;

namespace Common
{
    public class HorizontalMove : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        
        [SerializeField]
        private float direction = 1;

        private Rigidbody2D rb2D;

        void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
        }

        void Update()
        {
            rb2D.MovePosition(transform.position + (Vector3.right * speed * direction * Time.deltaTime));
        }

        public void SetDirection(float direction)
        {
            if (direction != this.direction)
            {
                var scale = transform.localScale;
                scale.x *= -1;
                this.direction *= -1;
                transform.localScale = scale;
            }
        }

        public void SetSpeed(float speed)
        {
            this.speed = speed;
        }
    }
}
