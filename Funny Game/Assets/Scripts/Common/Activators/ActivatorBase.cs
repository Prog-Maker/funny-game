﻿using UnityEngine;

namespace Common.Activators
{
    public abstract class ActivatorBase : MonoBehaviour
    {
        public abstract void Activate(Vector3 point);
    }
}
