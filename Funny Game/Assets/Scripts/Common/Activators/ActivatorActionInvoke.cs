﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.Events;

namespace Common.Activators
{
    public class ActivatorActionInvoke : ActivatorBase
    {
        [SerializeField]
        private UnityEvent action;

        [SerializeField]
        bool isSendMessage = true;

        public override void Activate(Vector3 point)
        {
            if (isSendMessage)
            {
                MMGameEvent.Trigger(GameEvents.GoodClick);
            }
            
            action.Invoke();
        }
    }
}
