﻿using UnityEngine;

namespace Common.Activators
{
    public class ActivatorDestroy : ActivatorBase
    {
        [SerializeField]
        private GameObject effect;
        public override void Activate(Vector3 point)
        {
            if (effect)
            {
               // Instantiate(effect, point, effect.transform.rotation);
                effect.transform.position = point;
                effect.GetComponent<ParticleSystem>().Play(true);
            }

            //Destroy(gameObject);
            DestroyMe();
        }

        public void DestroyMe()
        {
            gameObject.SetActive(false);
        }

        public void SetEffect(GameObject effect)
        {
            this.effect = effect;
        }

    }
}
