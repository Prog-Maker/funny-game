﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformObjectShow : MonoBehaviour
{

    void Awake()
    {
#if !UNITY_STANDALONE
        gameObject.SetActive(false);
#endif
    }
}
