﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Common
{
    public class Destroyer : MonoBehaviour
    {
        [SerializeField]
        private string scoreName = "MainScore";

        [SerializeField]
        private string exlosionKey = "explosion";

        public void DestroyMe()
        {
            Addressables.InstantiateAsync(exlosionKey, 
                                          new Vector3(transform.position.x, transform.position.y, 6),
                                          Quaternion.identity).Completed += DestroyWithExplosion;
        }

        private void DestroyWithExplosion(AsyncOperationHandle<GameObject>  asyncOperationHandle)
        {
            Addressables.ReleaseInstance(gameObject);
            MMGameEvent.Trigger(GameEvents.AddScore, scoreName, 1);
        }
    }
}
