﻿using UnityEngine;

namespace Common.EventSystem
{
    public class EventSender : MonoBehaviour
    {
        [SerializeField]
        private GameEvents gameEvents;

        [SerializeField]
        private string objectName = "";

        public void SendEvent()
        {
            MMGameEvent.Trigger(gameEvents, objectName, null);
        }

        public void SendEvent(string objectName)
        {
            MMGameEvent.Trigger(gameEvents, objectName, null);
        }

    }
}