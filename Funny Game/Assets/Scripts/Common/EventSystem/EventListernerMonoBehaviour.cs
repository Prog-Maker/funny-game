﻿using System;
using UnityEngine;

namespace Common.EventSystem
{
    public abstract class EventListernerMonoBehaviour : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        protected virtual void OnEnable()
        {
            this.MMEventStartListening();
        }

        protected virtual void OnDisable()
        {
            this.MMEventStopListening();
        }

        public abstract void OnMMEvent(MMGameEvent eventType);
    } 
}
