﻿using Common.Activators;
using Common.EventSystem;
using UnityEngine;

namespace Common
{
    public class CliclkDestroy : MonoBehaviour
    {
        [SerializeField]
        private LayerMask destroyMask;

        Camera cam;

        private void Start()
        {
            cam = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D hit = Physics2D.GetRayIntersection(ray, 20f, destroyMask);

                if (hit.collider != null)
                {
                    var entity = hit.collider.GetComponent<ActivatorBase>();
                    if (entity)
                    {
                        entity.Activate(hit.point);
                    }
                }
                else
                {
                    MMGameEvent.Trigger(GameEvents.LoseClick);
                }
            }
        }
    }
}