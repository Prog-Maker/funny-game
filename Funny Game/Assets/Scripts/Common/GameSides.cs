﻿using UnityEngine;

namespace Common
{
    public class GameSides : MonoBehaviour
    {
        [SerializeField]
        private Transform sideLeft, sideRight;

        [SerializeField]
        private float offset = 10;

        void Start()
        {
            if(ScreenParameters.Instance == null)
            {
                Debug.LogError("ScreenParameters instance is Null");
                return;
            }
            
            sideLeft.position = new Vector3(ScreenParameters.Instance.MinX - offset, sideLeft.position.y, sideLeft.position.z);
            sideRight.position = new Vector3(ScreenParameters.Instance.MaxX + offset, sideRight.position.y, sideRight.position.z);
        }
    }
}
