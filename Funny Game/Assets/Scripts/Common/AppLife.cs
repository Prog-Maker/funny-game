﻿using Common.EventSystem;
using UnityEngine;

namespace Common
{
    public class AppLife : EventListernerMonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                MMGameEvent.Trigger(GameEvents.EndGame);
            }
        }

        private void AppQuit()
        {
            Application.Quit();
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if(eventType.GameEventType == GameEvents.QuitGame)
            {
                AppQuit();
            }
        }
    }
}
