﻿using FunnyGame;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Common
{
    public class Spawner : MonoBehaviour, ISpawner
    {
        [SerializeField]
        private DifficultyLevel levelAsset;

        [SerializeField]
        private float offset_y, offset_x;

        private float minYPos, maxYPos, minXpos, maxXpos;

        private GameObject objectToSpawn;

        private int index = 0;

        private void Start()
        {
            minYPos = ScreenParameters.Instance.MinY + offset_y;
            maxYPos = ScreenParameters.Instance.MaxY - offset_y;
            minXpos = ScreenParameters.Instance.MinX - offset_x;
            maxXpos = ScreenParameters.Instance.MaxX + offset_x;
            
            InvokeRepeating(nameof(Spawn), 0, 1);
        }


        public void Spawn()
        {
            index = Random.Range(0, levelAsset.LevelAssets.Length);
            Addressables.InstantiateAsync(levelAsset.LevelAssets[index].assetReference).Completed += MoveToPosition;
        }

        private void MoveToPosition(AsyncOperationHandle<GameObject> asyncOperationHandle)
        {
            GameObject obj = asyncOperationHandle.Result;

            var rndDir = Random.Range(0, 2);

            float dir;

            float xPos;

            if (rndDir == 0)
            {
                xPos = minXpos;
                dir = 1;
            }
            else
            {
                xPos = maxXpos;
                dir = -1;
            }

            obj?.transform?.SetPositionAndRotation(new Vector3(xPos, Random.Range(minYPos, maxYPos), 0), Quaternion.identity);

            var horMove = obj?.GetComponent<HorizontalMove>();
            
            horMove?.SetDirection(dir);
            horMove?.SetSpeed(levelAsset.LevelAssets[index].speed);

            obj.SetActive(true);
        }
    }
}
