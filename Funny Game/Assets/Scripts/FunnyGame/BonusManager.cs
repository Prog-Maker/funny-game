﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace FunnyGame
{
    public class BonusManager : EventListernerMonoBehaviour
    {
        private float maxY;
        private float minX, maxX;

        void Start()
        {
            maxY = ScreenParameters.Instance.MaxY;
            minX = ScreenParameters.Instance.MinX;
            maxX = ScreenParameters.Instance.MaxX;
        }

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.GameEventType == GameEvents.Bonus)
            {
                Addressables.InstantiateAsync("bonus-box",
                                             new Vector2(Random.Range(minX + 5, maxX - 5), maxY + 2),
                                             Quaternion.identity);
            }
        }
    }
}
