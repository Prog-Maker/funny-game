﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.UI;

namespace FunnyGame
{
    public class GoodClicksSliderManager : EventListernerMonoBehaviour
    {
        [SerializeField]
        private Image fiil;

        [SerializeField]
        private Slider slider;

        private int count = 0;

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if(eventType.GameEventType == GameEvents.AddScore)
            {
                if(eventType.EventName == "Goodclicks")
                {
                    count = (int)eventType.Arg;

                    if (count > 0) 
                    { 
                        fiil.enabled = true;
                        slider.value = 0.2f * count;
                    }
                    else
                    {
                        slider.value = 0;
                        fiil.enabled = false;
                    }
                }
            }
        }

        void Start()
        {
            if (count == 0) fiil.enabled = false;
        }
    }
}
