﻿using Common.EventSystem;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class Returner : MonoBehaviour
{
    [SerializeField]
    private string scoreName = "MainScore";

    [SerializeField]
    private bool isAddScore = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Addressables.ReleaseInstance(collision.gameObject);
        if (isAddScore)
        {
            MMGameEvent.Trigger(GameEvents.AddScore, scoreName, (int)-1);
        }

    }
}
