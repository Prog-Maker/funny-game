﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace FunnyGame 
{
    [CreateAssetMenu(fileName = "DiffLevel", menuName = "Create New Diff Level")]
    public class DifficultyLevel : ScriptableObject
    {
        public LevelAsset[] LevelAssets;
    }

    [System.Serializable]
    public class LevelAsset
    {
        public AssetReference assetReference;

        public float speed;
    }
}